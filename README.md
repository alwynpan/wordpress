# wordpress #

Based on [wordpress official image](https://hub.docker.com/_/wordpress/).

### Usage ###

Please see [wordpress Docker hub page](https://hub.docker.com/_/wordpress/).

### Extra PHP Settings ###

* `PHP\_MEMORY\_LIMIT=...` (default to 64M)
* `PHP\_UPLOAD\_MAX\_FILESIZE=...` (default to 10M)
* `PHP\_POST\_MAX\_SIZE=...` (default to 10M)
* `PHP\_MAX\_EXECUTION\_TIME=...` (default to 300)